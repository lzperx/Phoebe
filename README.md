Ugrás a feladatokra: [![Stories in Ready](https://badge.waffle.io/lzperx/Phoebe.png?label=ready&title=Ready)](https://waffle.io/lzperx/Phoebe)
##Szoftver labor 4 feladatkiírás
`Phoebe`

A MarsOne telepesei szabad idejükben robotok versenyeit szervezik.
A robotok egy előre elkészített versenypályán ugrálnak. A pályáról leugró robotok elakadnak, és kiesnek a játékból.
A robotok álló helyzetből indulnak, minden robot a saját kezdőpozíciójából.
A sebességük egységnyi méretű, tetszőleges irányú sebességvektorral opcionálisan módosítható. Egy ugrással a sebességgel egyenesen arányos távolságra tudnak eljutni.
A pályán vannak olajfoltok, amikre érkezve sebességmódosításra nincs mód, illetve ragacsfoltok, amik a sebesség nagyságát megfelezik.
A robotok fel vannak szerelve olaj és ragacskészlettel, amiket a játékos parancsára elugráskor maguk mögött tudnak hagyni.
Az nyer, aki megadott idő alatt a legnagyobb távolságot tudja megtenni.

//Commit test
